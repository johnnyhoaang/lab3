//Johnny Hoang [2036759]
package LinearAlgebra;
public class MainVector { 
    public static void main(String[] args){
        Vector3d v1 = new Vector3d(2,4,6);
        Vector3d v2 = new Vector3d(4,7,9);
        System.out.println(v1.getX());
        System.out.println(v1.getY());
        System.out.println(v1.getZ());
        System.out.println(v1.magnitude());
        System.out.println(v1.dotProduct(v2));
        Vector3d v3 = v1.add(v2);
        System.out.print(v3);
    }
}