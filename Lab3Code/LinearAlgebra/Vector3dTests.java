//Johnny Hoang [2036759]
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {
    
    @Test
    public void testGetX()  {
        Vector3d v = new Vector3d(3,5,7);
        // x = 3
        assertEquals(v.getX(), 3);
    }
    @Test
    public void testGetY()  {
        Vector3d v = new Vector3d(3,5,7);
        // y = 5
        assertEquals(v.getY(), 5);
    }
    @Test
    public void testGetZ()  {
        Vector3d v = new Vector3d(3,5,7);
        // z = 7 
        assertEquals(v.getZ(), 7);
    }

    @Test
    public void testMagnitude() {
        Vector3d v  = new Vector3d(2,4,6);
        //Expected 7.483314773547883
        assertEquals(v.magnitude(), 7.483314773547883);
    }

    @Test 
    public void testDotProduct(){
        Vector3d v1 = new Vector3d(2,4,6);
        Vector3d v2 = new Vector3d(4,7,9);
        //Expected 90
        assertEquals(v1.dotProduct(v2), 90);
    }

    @Test
    public void add(){
        Vector3d v1 = new Vector3d(2,4,6);
        Vector3d v2 = new Vector3d(4,7,9);
        Vector3d newVector = v1.add(v2);
        //Expected Vector3d Object containing (6,11,15)
        assertEquals(newVector.getX(), 6);
        assertEquals(newVector.getY(), 11);
        assertEquals(newVector.getZ(), 15);
    }


}
